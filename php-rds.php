<?php

$server = "";
$user = "admin";
$password = "";
$dbname = "test_db";
//create connection
$conn = new mysqli($server,$user,$password,$dbname);

if ($conn->connect_error){
    die("Connection to server $server failed.");
}
else {
    echo("Connection to $server succesful\n");}


$sql = "CREATE Database IF NOT EXISTS $dbname";
$retval = $conn->query($sql);
echo "return value : $retval\n";

echo "Succesfully created a database\n";

//create tables

$sql = "CREATE TABLE MyGuests (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
email VARCHAR(50),
reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)";

if ($conn->query($sql) === TRUE) {
    echo "Table MyGuests created successfully\n";
} else {
    echo "Error creating table: " . $conn->error ."\n";
}

$sql = "INSERT INTO MyGuests (firstname, lastname, email)
	VALUES ('Johnny', 'van Veen', 'john.52@a.txt')";

$conn->query($sql);


$sql = "SELECT * from MyGuests";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>\n";
    }
} else {
    echo "0 results";
}

$conn->close()


?>
