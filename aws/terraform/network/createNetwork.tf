provider "aws" {
  region = "us-east-1"
  version = "2.55"
}

data "aws_availability_zones" "available" {
  state = "available"
}


resource "aws_vpc" "mainVPC" {
  cidr_block = "192.168.0.0/16"
  enable_dns_hostnames = "true"

  tags = {
    Name = "TfMainVpc"
  }
}

resource "aws_subnet" "publicSub1" {
  vpc_id = aws_vpc.mainVPC.id
  cidr_block = "192.168.1.0/24"
  availability_zone = data.aws_availability_zones.available.names[0]
  tags = {
    Name = "publicSub1"
  }
}

resource "aws_subnet" "publicSub2" {
  vpc_id = aws_vpc.mainVPC.id
  cidr_block = "192.168.2.0/24"
  availability_zone = data.aws_availability_zones.available.names[1]
  tags = {
    Name = "publicSub2"
  }
}

resource "aws_subnet" "privateSub1" {
  vpc_id = aws_vpc.mainVPC.id
  cidr_block = "192.168.10.0/24"
  availability_zone = data.aws_availability_zones.available.names[0]
  tags = {
    Name = "privateSub1"
  }
}

resource "aws_subnet" "privateSub2" {
  vpc_id = aws_vpc.mainVPC.id
  cidr_block = "192.168.11.0/24"
  availability_zone = data.aws_availability_zones.available.names[1]
  tags = {
    Name = "privateSub2"
  }
}

#create IGW
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.mainVPC.id

  tags = {
    Name = "main"
  }
}

#route tables##

resource "aws_route_table" "publicR" {
  vpc_id = aws_vpc.mainVPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "PublicRouteTable"
  }
}

resource "aws_route_table" "privateR" {
  vpc_id = aws_vpc.mainVPC.id

  tags = {
    Name = "PrivateRouteTable"
  }
}

resource "aws_route_table_association" "routePubAsso" {
  subnet_id      = aws_subnet.publicSub1.id
  route_table_id = aws_route_table.publicR.id
}



resource "aws_security_group" "sg-ssh-allow" {
  name = "Allow_ssh"
  description = "Test sg"
  vpc_id = aws_vpc.mainVPC.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    }
    
  tags = {
    Name = "sg-ssh-allow"
  }
}
