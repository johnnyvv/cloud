variable "aws_main_region" {
  type    = string
  default = "us-east-1"
}

variable "default_identifier_tag" {
  type        = map
  description = "default tag to add to resources"
  default     = { 
              deployment = "tf-jvv"
              }
}

##network variables##

variable "vpc_cidr" {
  type        = string
  description = "cidr block for the vpc"
  default     = "192.168.0.0/16"  #subnets will have /24 networks 
}

variable "public_subnets" {
  type    = map
  default = {
    "pub1" = {
       "network"  = 0
       "az"       = 0
      }
    "pub2" = { 
       "network"  = 1
       "az"       = 1
      }
  }
}

variable "private_subnets" {
  type    = map
  default = {
    "private1" = {
       "network"  = 10
       "az"       = 0
      }
    "private2" = { 
       "network"  = 11
       "az"       = 1
     }
  }
}
