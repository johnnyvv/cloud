provider "aws" {
  region  = var.aws_main_region
  version = "2.55"
}
