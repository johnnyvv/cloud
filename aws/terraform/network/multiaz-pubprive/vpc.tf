# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr 
  enable_dns_hostnames = "true"
 
  tags = merge(var.default_identifier_tag,{
    Name = "mainVpc" }
    )
}
  
resource "aws_subnet" "public" {
  for_each                  = var.public_subnets
    vpc_id                  = aws_vpc.main.id
    cidr_block              = cidrsubnet(aws_vpc.main.cidr_block, 8, each.value.network) #vpc cidr is 16 so +8 makes subnet cidr 24
    availability_zone       = data.aws_availability_zones.available.names[each.value.az]
    map_public_ip_on_launch = true   
    tags = merge(var.default_identifier_tag,{
      Name = each.key
    })
}

resource "aws_subnet" "private" {
  for_each                  = var.private_subnets
    vpc_id                  = aws_vpc.main.id
    cidr_block              = cidrsubnet(aws_vpc.main.cidr_block, 8, each.value.network) #vpc cidr is 16 so +8 makes subnet cidr 24
    availability_zone       = data.aws_availability_zones.available.names[each.value.az]
    map_public_ip_on_launch = false

    tags = merge(var.default_identifier_tag,{
      Name = each.key
    })
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
  
  tags = merge(var.default_identifier_tag,{
    Name = "MainIGW" }
    )
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = merge(var.default_identifier_tag,{
    Name = "Public" }
    )
}

resource "aws_route_table_association" "public" {
  for_each = var.public_subnets
    subnet_id = aws_subnet.public[each.key].id
    route_table_id = aws_route_table.public.id
}

resource "aws_security_group" "ssh-allow" {
  name        = "Allow_ssh"
  description = "Test sg"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    }

  tags = var.default_identifier_tag
}
