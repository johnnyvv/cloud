##SCRIPT THAT YOU CAN ONLY RUN WHEN YOU JUST CREATED THE SANDBOX ENVIRONMENT##
##THERE IS NO INTENSE ERROR CHECKING / CORRECTION##
##CREATE A VPC - 2 SUBNETS PUBLIC,PRIVATE - 2 ROUTING TABLES, 2 NACL, ASSOCIATE SUBNET WITH NACL, IGW, ADD ROUTE 0.0.0.0/0 FOR PUBLIC TO IGW, NACL RULE SSH , SG PUBLICH WITH ALLOW SSH

region="us-east-1"
profile_name=sandbox
output_default=json

cidr_block="10.0.0.0/24"
public_sub="10.0.0.0/28"
private_sub="10.0.0.16/28"

export AWS_PROFILE=$profile_name

#need to check, there might be a default one.. 
echo "Creating VPC"
if $(aws ec2 create-vpc --cidr-block $cidr_block --no-amazon-provided-ipv6-cidr-block --region $region > /dev/null 2>&1) ; then
  echo "Done"
else
  echo "Failed to create VPC, exiting"
  exit 1
fi

vpc_id=$(aws ec2 describe-vpcs --filter "Name='cidr',Values='${cidr_block}'" --query "Vpcs[*].VpcId" --output text)

echo "Creating public subnet"
if $(aws ec2 create-subnet --cidr-block $public_sub --vpc-id $vpc_id > /dev/null 2>&1) ; then echo "Done" ; else echo "Failed to create public subnet" && exit ; fi
echo "Creating private subnet"
if $(aws ec2 create-subnet --cidr-block $private_sub --vpc-id $vpc_id > /dev/null 2>&1) ; then echo "Done" ; else echo "Failed to create private subnet" && exit ; fi

public_subnet_id=$(aws ec2 describe-subnets --filter "Name='cidr-block',Values='${public_sub}'" --query "Subnets[*].SubnetId"  --output text) #could make one liners..  to
private_subnet_id=$(aws ec2 describe-subnets --filter "Name='cidr-block',Values='${private_sub}'" --query "Subnets[*].SubnetId"  --output text)
#create tag for subnets Name=private|public etc..


#now you have the subnetIds you can assign them to a routing table. Obviously we want the private to not have an entry to the internet.. Also we need an IGW
echo "Creating public route table"
if $(aws ec2 create-route-table --vpc-id=$vpc_id --query "RouteTable.RouteTableId" --output text > /dev/null 2>&1) ; then echo "Done" ; else echo "Failed to create route table" && exit ; fi
echo "Creating private route table"
if $(aws ec2 create-route-table --vpc-id=$vpc_id --query "RouteTable.RouteTableId" --output text > /dev/null 2>&1) ; then echo "Done" ; else echo "Failed to create route table" && exit ; fi

public_route_table=$(aws ec2 create-route-table --vpc-id=$vpc_id --query "RouteTable.RouteTableId" --output text)
private_route_table=$(aws ec2 create-route-table --vpc-id=$vpc_id --query "RouteTable.RouteTableId" --output text)

#assign subnets to correct routing table
#if $(aws ec2 associate-route-table --route-table-id $public_route_table --subnet-id $public_subnet_id > /dev/null 2>&1); then echo "Done" ; else echo "Failed to assosicate public subnet to rt" && exit ; fi
#if $(aws ec2 associate-route-table --route-table-id $private_route_table --subnet-id $private_subnet_id > /dev/null 2>&1); then echo "Done" ; else echo "Failed to assosicate public subnet to rt" && exit ; fi

echo "Assigning subnets to route tables"
aws ec2 associate-route-table --route-table-id $public_route_table --subnet-id $public_subnet_id
aws ec2 associate-route-table --route-table-id $private_route_table --subnet-id $private_subnet_id

echo "Creating Internet Gateway"
igw_id=$(aws ec2 create-internet-gateway --query "InternetGateway.InternetGatewayId" --output text)

echo "Attaching public rt to igw"
aws ec2 attach-internet-gateway --vpc-id $vpc_id --internet-gateway-id $igw_id > /dev/null

echo "Adding internet route to public rt"
aws ec2 create-route --destination-cidr-block "0.0.0.0/0" --gateway-id $igw_id --route-table-id $public_route_table

echo "Creating public / private NACL"
public_nacl_id=$(aws ec2 create-network-acl --vpc-id $vpc_id --query "NetworkAcl.NetworkAclId" --output text)
private_nacl_id=$(aws ec2 create-network-acl --vpc-id $vpc_id --query "NetworkAcl.NetworkAclId" --output text)


echo "Assigning subnets to NACLs"
asso_id_public=$(aws ec2 describe-network-acls --filter "Name='association.subnet-id',Values='${public_subnet_id}'" --query "NetworkAcls[*].Associations[*]" | jq  '.[][] | select (.SubnetId==env.public_subnet_id)' | jq -r .NetworkAclAssociationId)
aws ec2 replace-network-acl-association --association-id $asso_id_public --network-acl-id $public_nacl_id
#add private to private acl, note about the environment variables
asso_id_private=$(aws ec2 describe-network-acls --filter "Name='association.subnet-id',Values='${private_subnet_id}'" --query "NetworkAcls[*].Associations[*]" | jq  '.[][] | select (.SubnetId==env.private_subnet_id)' | jq -r .NetworkAclAssociationId)
aws ec2 replace-network-acl-association --association-id $asso_id_private --network-acl-id $private_nacl_id


echo "Creating SSH rule for public NACL"
aws ec2 create-network-acl-entry --rule-number 10 --rule-action allow --cidr-block "0.0.0.0/0" --protocol tcp --port-range From=22,To=22  --network-acl-id $public_nacl_id --ingress
echo "Creating all traffic egress rule for public NACL"
aws ec2 create-network-acl-entry --rule-number 10 --rule-action allow --cidr-block "0.0.0.0/0" --protocol '-1' --network-acl-id $public_nacl_id --egress
echo "Creating security group for public"
sg_id_public=$(aws ec2 create-security-group --description public-security-group --group-name public --vpc-id $vpc_id --output text)
aws ec2 authorize-security-group-ingress --group-id $sg_id_public --protocol tcp --port 22 --cidr '0.0.0.0/0'
echo "Creating SSH rule for public SG"
