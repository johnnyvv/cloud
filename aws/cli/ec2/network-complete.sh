#need to add some logic and error checking

akey=#dont forget, dont upload your keys!
skey=#dont forget, dont upload your keys!
region="us-east-1" 

cidr_block="10.0.0.0/16"
public_sub="10.0.0.0/28"
private_sub="10.0.0.16/28"

#create a vpc 
aws ec2 create-vpc --cidr-block $cidr_block --no-amazon-provided-ipv6-cidr-block --region $region


vpc_id=$(aws ec2 describe-vpcs --filter "Name='cidr',Values='${cidr_block}'" --query "Vpcs[*].VpcId" --output text)

#create 2 subnet
aws ec2 create-subnet --cidr-block $public_sub --vpc-id $vpc_id

aws ec2 create-subnet --cidr-block $private_sub --vpc-id $vpc_id


#get subnet ids
public_subnet_id=$(aws ec2 describe-subnets --filter "Name='cidr-block',Values='${public_sub}'" --query "Subnets[*].SubnetId"  --output text)
private_subnet_id=$(aws ec2 describe-subnets --filter "Name='cidr-block',Values='${private_sub}'" --query "Subnets[*].SubnetId"  --output text)



#now you have the subnetIds you can assign them to a routing table. Obviously we want the private to not have an entry to the internet.. Also we need an IGW
public_route_table=$(aws ec2 create-route-table --vpc-id=$vpc_id --query "RouteTable.RouteTableId" --output text)
private_route_table=$(aws ec2 create-route-table --vpc-id=$vpc_id --query "RouteTable.RouteTableId" --output text) 


#assign subnets to correct routing table
aws ec2 associate-route-table --route-table-id $public_route_table --subnet-id $public_subnet_id
aws ec2 associate-route-table --route-table-id $private_route_table --subnet-id $private_subnet_id

#when you need another one than the default one created with the vpc
#aws ec2 create-network-acl --vpc-id $vpc_id

#create internet gateway
igw_id=$(aws ec2 create-internet-gateway --query "InternetGateway.InternetGatewayId" --output text)


#attach igw to vpc
aws ec2 attach-internet-gateway --vpc-id $vpc_id --internet-gateway-id $igw_id

#add igw to public subnet with route of 0.0.0.0/0
aws ec2 create-route --destination-cidr-block "0.0.0.0/0" --gateway-id $igw_id --route-table-id $public_route_table


#add network acl with rules

#add security group with rules
