akey=$1
skey=$2
region="us-east-1"

profile_name=<name of the profile, just an identifier, different from username>
output_default=json #text table or json you can always overwrite this settings by using <command> --output X
#configure CLI
#if [[ -d $HOME/.aws ]]; then
#    printf "[sandbox]\naws_access_key_id=${akey}\naws_secret_access_key=${skey}" >> $HOME/.aws/credentials #need to find a way to check for existing profile with same name
#    printf "[sandbox]\nregion=${region}\noutput=${output_default}" >> $HOME/.aws/config
#deleting awk folder, comment out if not needed
rm -rf $HOME/.aws
unset AWS_PROFILE
aws configure set aws_access_key_id $akey --profile $profile_name
aws configure set aws_secret_access_key $skey --profile $profile_name
aws configure set region $region --profile $profile_name
aws configure set output $output_default --profile $profile_name
export AWS_PROFILE=$profile_name

#usage source <script> <access key> <secret key> #by using source, it will set your current env good as well.. in case you need to troubleshoot.. which you probably will