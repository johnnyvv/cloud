#!/bin/env bash

#no support now for AWS TOKEN
#invoke like
if [[ -z $1 ]] ; then
  echo "First argument needs to be access key id! Exiting..."
  echo "Usage: source ${0} access_key_id [region]"
  return 1 
else
  AWS_ACCESS_KEY_ID=$1 #first param sets key id
  AWS_REGION=${2:-us-east-1} #second optional and sets region
  echo -en "Enter secret key:\n"
  read -s AWS_SECRET_ACCESS_KEY

  export AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_REGION 
fi
