//write event from S3 to dynamodb table
package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func Handler(ctx context.Context, s3Event events.S3Event) {

	svc := dynamodb.New(session.New())

	for _, record := range s3Event.Records {
		//fmt.Println(record)
		fmt.Println("region:", record.AWSRegion)
		fmt.Println("bucket:", record.S3.Bucket)
		fmt.Println("event:", record.EventName)
		fmt.Println("key:", record.S3.Object.Key)
	}
}

func main() {
	lambda.Start(Handler)
}
