//get a key as input -> check if key exists in bucket (env var) -> spit to screen or return error

package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type MyEvent struct {
	Key string `json:"key"`
}

func HandleRequest(ctx context.Context, name MyEvent) (string, error) {

	//check if env var exists
	var bucket string
	if _, exists := os.LookupEnv("BUCKET"); exists {
		bucket = os.Getenv("BUCKET")
	} else {
		log.Fatal("BUCKET env variable not set. Exiting...")
		return "", errors.New("Env var BUCKET not found")
	}

	svc := s3.New(session.New())
	if !checkKeyExists(svc, bucket, name.Key) {
		return fmt.Sprintf("Key: %s does not exist", name.Key), errors.New("File does not exist in bucket")
	}
	return fmt.Sprintf("Key: %s does exist", name.Key), nil
	//return fmt.Sprintf("Hello %s!", name.Key), nil
}

func main() {
	lambda.Start(HandleRequest)
}

func checkKeyExists(svc *s3.S3, bucket, key string) bool {
	obj, err := svc.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		fmt.Println("Error heading file:", err)
		return false
	}
	fmt.Println(obj)
	return true
}
