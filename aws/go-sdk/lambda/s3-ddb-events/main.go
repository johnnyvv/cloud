//when item put in s3, will generate presigned url for x seconds (default / env variable)
//put objectKey, bucket name/region, signed url, timespamp and status to ddb
//later scheduled lambda to check if link is still failed, if not set status to expired and remove the urlString value
package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/s3"
)

type Item struct {
	BucketName   string        `json:"bucketName"`
	BucketRegion string        `json:"bucketRegion"`
	ObjectKey    string        `json:"objectKey"`
	GeneratedAt  int64         `json:"generatedAt"`
	UrlDuration  time.Duration `json:"urlDuration"`
	SignedUrl    string        `json:"signedUrl"`
	UrlStatus    string        `json:"urlStatus"`
}

func Handler(ctx context.Context, s3Event events.S3Event) {

	tmpItem := Item{}
	//read events
	for _, record := range s3Event.Records {
		//need to make dynamic later.. for now I dunno
		//fmt.Println(record)
		tmpItem.BucketName = record.S3.Bucket.Name
		tmpItem.BucketRegion = record.AWSRegion
		tmpItem.ObjectKey = record.S3.Object.Key
		//fmt.Println("bucket:", record.S3.Bucket)
		//fmt.Println("event:", record.EventName)

	}
	//if you dont have proper s3 permissions, the url will be generated but will give access denies!
	tmpItem.createPresignedUrl()
	//fmt.Println(tmpItem.SignedUrl)

	//write to dynamodb
	tableName := "s3urls" //later make this dynamic -> param store or env variable
	svc := dynamodb.New(session.New())
	//create input for putting to dynamodb
	ddbItem, err := dynamodbattribute.MarshalMap(tmpItem)
	if err != nil {
		fmt.Println("Something wrong marshaling struct:", err)
		os.Exit(1)
	}
	input := &dynamodb.PutItemInput{
		TableName: aws.String(tableName),
		Item:      ddbItem,
	}
	_, err = svc.PutItem(input)
	if err != nil {
		fmt.Println("Something wrong writing to ddb:", err)
		os.Exit(1)
	}

}

func main() {
	lambda.Start(Handler)
}

func (i *Item) createPresignedUrl() {
	i.UrlDuration = 900
	timeNow := time.Now()
	epochTime := timeNow.Unix()
	//need to think about how to put it in the init function like AWS tells you to
	svc := s3.New(session.New())
	req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(i.BucketName),
		Key:    aws.String(i.ObjectKey),
	})
	urlStr, err := req.Presign(15 * time.Minute) //sign url for 15 minutes
	if err != nil {
		//fmt.Println("Something went wrong presigning url:", err)
		i.GeneratedAt = epochTime
		fmt.Println(errors.New("Something went wrong generating url for object:" + i.ObjectKey))
		os.Exit(1)
	}
	i.SignedUrl = urlStr
	i.GeneratedAt = epochTime
}
