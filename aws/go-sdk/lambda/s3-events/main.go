//work with s3 events
package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func Handler(ctx context.Context, s3Event events.S3Event) {
	for _, record := range s3Event.Records {
		//fmt.Println(record)
		fmt.Println("region:", record.AWSRegion)
		fmt.Println("bucket:", record.S3.Bucket)
		fmt.Println("event:", record.EventName)
		fmt.Println("key:", record.S3.Object.Key)
	}
}

func main() {
	lambda.Start(Handler)
}
