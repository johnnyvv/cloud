package main

//compile it, zip it, upload it
import (
//	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
)

type Request struct {
	Name string `json:"name"`
}

type Response struct {
	Message string `json:"message"`
	Ok	bool   `json:"ok"`
}
/*
func HandleRequest(ctx context.Context, request Request) (string, error) {
	fmt.Println(ctx)
	fmt.Printf("%T\n",ctx)
	return fmt.Sprintf("Hello %s!", request.Name), nil
}
*/

func HandleRequest(request Request) (Response, error){
	return Response {
		Message: fmt.Sprintf("received request from: %s",request.Name),
		Ok: true},nil
	
}


func main() {
	lambda.Start(HandleRequest)
}
