//takes key to create pre-signed url for
package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type Request struct {
	Key string `json:"key"`
}

type Response struct {
	Message string `json:"message"`
	Url     string `json:"url"`
	Ok      bool   `json:"ok"`
}

func HandleRequest(request Request) (Response, error) {

	var bucket string
	if _, exists := os.LookupEnv("BUCKET"); exists {
		bucket = os.Getenv("BUCKET")
	} else {
		log.Fatal("BUCKET env variable not set. Exiting...")
		return Response{
			Message: "bucket env var not set",
			Url:     "",
			Ok:      false}, errors.New("Env var BUCKET not found")
	}

	svc := s3.New(session.New())

	if !checkKeyExists(svc, bucket, request.Key) {
		return Response{
			Message: fmt.Sprintf("Key %s does not exist in bucket %s", request.Key, bucket),
			Url:     "",
			Ok:      false}, errors.New("key does not exist in bucket")
	}

	return request.createPresignedUrl(svc, bucket)
}

func checkKeyExists(svc *s3.S3, bucket, key string) bool {
	obj, err := svc.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		fmt.Println("Error heading file:", err)
		return false
	}
	fmt.Println(obj)
	return true
}

func (r *Request) createPresignedUrl(svc *s3.S3, bucket string) (Response, error) {
	//create presigned url
	req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(r.Key),
	})
	urlStr, err := req.Presign(15 * time.Minute) //sign url for 15 minutes
	if err != nil {
		return Response{
			Message: "failed to create presigned url",
			Url:     "",
			Ok:      false}, errors.New("failed to create signed url")
	}

	return Response{
		Message: fmt.Sprintf("succesfully created url for key %s", r.Key),
		Url:     urlStr,
		Ok:      true}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
