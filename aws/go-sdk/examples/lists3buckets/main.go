package main

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

func main() {
	//https://docs.aws.amazon.com/sdk-for-go/api/aws/session/
	//create a session that uses AWS_REGION AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY or shared cred file
	sess, err := session.NewSession()
	if err != nil {
		fmt.Printf("Error:%v", err)
		os.Exit(1)
	}
	//create s3 client instance from session
	svc := s3.New(sess)
	fmt.Printf("%T\n", svc)
	buckets, err := svc.ListBuckets(nil)
	if err != nil {
		os.Exit(1)
	}
	for _, b := range buckets.Buckets {
		fmt.Println(*b.Name)
	}

	//create a bucket
	newBucketName := ""
	_, err = svc.CreateBucket(&s3.CreateBucketInput{Bucket: aws.String(newBucketName)})
	if err != nil {
		fmt.Println("Failed to create bucket:", err)
	}

	listBuckets(svc)
}

func listBuckets(svc *s3.S3) {
	buckets, err := svc.ListBuckets(nil)
	if err != nil {
		fmt.Println("Failed to list buckets. Error:", err)
	} else {
		for _, b := range buckets.Buckets {
			fmt.Println(*b.Name)
		}
	}

}
