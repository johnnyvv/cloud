package main

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

func main() {
	sess, err := session.NewSession()
	if err != nil {
		fmt.Printf("Error:%v", err)
		os.Exit(1)
	}
	svc := s3.New(sess)
	bucketName := "tmpbucket007"

	x := checkKeyExists(svc, bucketName, "flask-stress.txt")
	fmt.Println(x)

}

func checkKeyExists(svc *s3.S3, bucket, key string) bool {
	obj, err := svc.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		fmt.Println("Error heading file:", err)
		return false
	}
	fmt.Println(obj)
	return true
}
