package main

import (
	"fmt"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func main() {
	sess, err := session.NewSession()
	if err != nil {
		fmt.Printf("Error:%v", err)
		os.Exit(1)
	}
	svc := s3.New(sess)

	bucketName := "delmejvv"

	//first create a file + contents and upload it
	fileName := "myfile.txt"
	d1 := []byte("Hello from my file\nPlease feel free to download me, if permitted..")
	f, err := os.Create(fileName)
	if err != nil {
		fmt.Println("Something wrong openening file:", err)
	}
	defer func() {
		fmt.Println("Closing file:", fileName)
		f.Close()
	}()
	_, err = f.Write(d1)
	if err != nil {
		fmt.Println("Failed to write to file:", err)
	}

	// now upload the file to s3
	uploader := s3manager.NewUploader(sess)

	f, err = os.Open(fileName)
	if err != nil {
		fmt.Println("Something wrong opening file:", err)
	}

	_, err = uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(f.Name()),
		Body:   f,
	})

	if err != nil {
		fmt.Println("Something went wrong uploading to s3:", err)
		os.Exit(1)
	}

	//create presigned url
	req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(fileName),
	})
	urlStr, err := req.Presign(15 * time.Minute) //sign url for 15 minutes
	if err != nil {
		fmt.Println("Something went wrong presigning url:", err)
	}
	fmt.Println("Presign url is:\n", urlStr)

}
