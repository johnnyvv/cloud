//put / get item(s) to DDB
package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type Item struct {
	BucketName   string `json:"bucketName"`
	ObjectKey    string `json:"objectKey"`
	BucketRegion string `json:"bucketRegion"`
}

func main() {

	tableName := "s3urls"
	//dynamoDBTableName, err := checkOSEnv("TABLE_NAME")
	//check(err)
	fmt.Println("Using table:", tableName)

	svc := dynamodb.New(session.New())
	AWSRegion := svc.SigningRegion

	item := Item{
		BucketName:   "my_bucket",
		ObjectKey:    "install.txt",
		BucketRegion: AWSRegion,
	}

	av, err := dynamodbattribute.MarshalMap(item)
	check(err)
	//fmt.Println(av)
	//create input to put to the table
	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}
	_, err = svc.PutItem(input)
	check(err)
	//get item from table

	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"bucketName": {
				S: aws.String(item.BucketName),
			},
			"objectKey": {
				S: aws.String("install.txt"),
			},
		},
	})
	fmt.Println(result)
	check(err)
	fmt.Printf("%T\n", result.Item)

	x := Item{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &x)
	check(err)
	fmt.Println(item)
	//query table
	fmt.Println("see me?")
	queryResult, err := svc.Query(&dynamodb.QueryInput{
		TableName: aws.String(tableName),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":v1": {
				S: aws.String(item.BucketName),
			},
		},
		KeyConditionExpression: aws.String("bucketName = :v1"),
	})
	check(err)
	for _, result := range queryResult.Items {
		//fmt.Println(result.
		tmpItem := Item{}
		err := dynamodbattribute.UnmarshalMap(result, &tmpItem)
		check(err)
		fmt.Println(tmpItem.ObjectKey)
	}

	queryResult, err = svc.Query(&dynamodb.QueryInput{
		TableName: aws.String(tableName),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":v1": {
				S: aws.String(item.BucketName),
			},
			":v2": {
				S: aws.String(item.ObjectKey),
			},
		},
		KeyConditionExpression: aws.String("bucketName = :v1 and objectKey = :v2"),
	})
	check(err)
	for _, result := range queryResult.Items {
		//fmt.Println(result.
		tmpItem := Item{}
		err := dynamodbattribute.UnmarshalMap(result, &tmpItem)
		check(err)
		fmt.Println(tmpItem.ObjectKey)
	}

}

func checkOSEnv(envVar string) (string, error) {
	var retStr string
	if _, exists := os.LookupEnv(envVar); exists {
		retStr = os.Getenv(envVar)
		return retStr, nil
	}
	return retStr, errors.New(fmt.Sprintf("ENV var %s not found", envVar))
}

func check(err error) {
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
}
