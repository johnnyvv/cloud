package main

import (
	"bytes"
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func main() {
	sess, err := session.NewSession()
	if err != nil {
		fmt.Printf("Error:%v", err)
		os.Exit(1)
	}
	svc := s3.New(sess)

	bucketName := "mytestbucket-jvv"

	//get object
	file, _ := os.Create("test.txt")
	objectKey := "s3.json"
	downloader := s3manager.NewDownloader(sess)
	num, err := downloader.Download(file, &s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	})
	if err != nil {
		fmt.Println("Error downloading file:", err)
		os.Exit(1)
	}
	fmt.Printf("Downloaded file %s -> file size: %v\n", file.Name(), num)

	resp, err := svc.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	})
	if err != nil {
		fmt.Println("Error downloading file:", err)
		os.Exit(1)
	}
	//https://golangcode.com/convert-io-readcloser-to-a-string/
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	fmt.Println(buf.String())

	//lets upload some files
	uploader := s3manager.NewUploader(sess)

	f, err := os.Open("myinput.txt")
	if err != nil {
		fmt.Println("Something wrong opening file:", err)
	}

	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(f.Name()),
		Body:   f,
	})
	if err != nil {
		fmt.Println("Failed to upload file:", err)
	}
	fmt.Println(result.Location)
	//list bucket contents
	listBucketObjects(svc, bucketName)

}

func listBucketObjects(svc *s3.S3, bucket string) {
	//list items currently in bucket
	bucketContent, err := svc.ListObjects(&s3.ListObjectsInput{Bucket: aws.String(bucket)})
	if err != nil {
		fmt.Println("Error listing bucket ", bucket)
	}
	for _, o := range bucketContent.Contents {
		fmt.Println(*o.Key)
	}
	fmt.Println("Objects in bucket:", len(bucketContent.Contents))
}
