import boto3, json
from time import sleep
from os import environ
from dtest import Dynamodb_helper

def get_parameters_r_dict(list_params):
    #input a list of parameters you want to receive, return a dict with key = name of param, and value = value of param
    if list_params and isinstance(list_params, list):
        r_dict = {}
        param_client = boto3.client('ssm', region_name=environ.get('AWS_REGION','us-east-1'))
        params = param_client.get_parameters(Names=list_params)

        for param in params['Parameters']:
            r_dict[param['Name']] = param['Value']
            print(param)

        return r_dict
    #need to add some raise or smth

def main():

    #pylint: disable=no-member
    time_sleep = 20

    params = get_parameters_r_dict(['s3_queue_name','s3_bucket_name_output','s3_dynamo_table_name'])
    sqs_queue = params.get('s3_queue_name')
    s3_output_bucket = params.get('s3_bucket_name_output')
    dynamo_table_name = params.get('s3_dynamo_table_name')

    sqs = boto3.resource('sqs', region_name=environ.get('AWS_REGION','us-east-1'))
    sqs.get_queue_by_name(QueueName=sqs_queue)
    sqs_url = sqs.get_queue_by_name(QueueName=sqs_queue).url

    s3 = boto3.resource('s3')
    output_bucket = s3.Bucket(s3_output_bucket)
    del s3

    queue = sqs.Queue(sqs_url)
    max_msgs = 10

    #set up dynamodb helpder
    dynamo = Dynamodb_helper(dynamo_table_name,'MessageId')

    #checking queue
    client = boto3.client('sqs', region_name=environ.get('AWS_REGION','us-east-1'))
    queue_attr = client.get_queue_attributes(
        QueueUrl=sqs_url,
        AttributeNames=['ApproximateNumberOfMessages'])

    print(f"Items in queue: {queue_attr['Attributes']['ApproximateNumberOfMessages']}")
    #checking for messages

    #connecting to s3 bucket to read message
    s3c = boto3.client('s3')

    while True:
        for message in queue.receive_messages(
                MaxNumberOfMessages=max_msgs):
                
            print(f"Processing message {message.message_id}") 
            body = json.loads(message.body)
            dynamo.add_message(message.message_id) 
            if "Records" in body:
                print(f"object in bucket {body['Records'][0]['s3']['bucket']['name']} - {body['Records'][0]['s3']['object']['key']}")
                #create an object from the bucket name and key in the message - 
                obj = s3c.get_object(
                        Bucket=body['Records'][0]['s3']['bucket']['name'],
                        Key=body['Records'][0]['s3']['object']['key']
                        )

                output_bucket.put_object(
                        Key=body['Records'][0]['s3']['object']['key'],
                        Body=obj['Body'].read().upper()
                        )
                dynamo.messaged_processed(
                    message.message_id,
                    body['Records'][0]['s3']['bucket']['name'],
                    s3_output_bucket,
                    body['Records'][0]['s3']['object']['key'])
                message.delete()
            
            elif "Event" in body:
                if body['Event'] == "s3:TestEvent":
                    print("Test message found")
                    print(f"Test message from {body['Service']} at {body['Time']}")
                    message.delete()
                    print("Deleted")
            
            else:
                print("Did not find s3 in message, please check if processing is needed")
                print(json.dumps(body,indent=4, sort_keys=True))
    sleep(time_sleep)


if __name__ == '__main__':
    main()
"""
    obj = s3.head_object(
            Bucket='testbucket10101x',
            Key=str("xbun/xbun-21-36-26-03-2020.txt")
            )
"""
