import json, boto3
import string, random

from os import environ
from time import time #to get epoch 

class Dynamodb_helper():
    
    N = 10 #random string chars
    #client = boto3.client('dynamodb')

    def __init__(self,table_name,p_key):
        self.client = boto3.client('dynamodb',region_name=environ.get('AWS_REGION','us-east-1')) #perhaps need to make class var from this
        self.table_name = table_name
        self.p_key = p_key
    
    def get_item(self,message_id):
        #will get item from dynamodb
        response = self.client.get_item(TableName=self.table_name,Key={
            self.p_key : {'S': message_id}
            },
            AttributesToGet=[
                self.p_key,
                'itemAdded'
                ]
            )
        print(response)
        
    def add_message(self,message_id):
        #put item in dynamodb
        #message_id = ''.join(random.choices(string.ascii_uppercase + string.digits, k=Dynamodb_helper.N))
        response = self.client.put_item(TableName=self.table_name,Item={
            self.p_key : {'S': message_id},
            'stage': { 'S': 'registered'},
            'itemAdded' : {'S': str(int(time()))}
        })
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            print(f"Succesfully written message {message_id} to table {self.table_name}")
        else:
            print("Something went wrong putting item to table")
            print(response)

    def messaged_processed(self,message_id,source_bucket,dest_bucket,object_key):
        #change stage of message and add some atributes for the bucket / object
        response = self.client.update_item(TableName=self.table_name,Key={
            self.p_key : {'S': message_id}
            },
            UpdateExpression="SET stage=:value1,source_bucket=:value2,dest_bucket=:value3,object_key=:value4",
            ExpressionAttributeValues={
                ':value1' : {'S': 'processed'},
                ':value2' : {'S': source_bucket},
                ':value3' : {'S': dest_bucket},
                ':value4' : {'S': object_key}
            }
        )
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            print(f"Succesfully updated message {message_id} on table {self.table_name}")
        else:
            print("Something went wrong updating item with key {message_id}")
            print(response)         

    
    def update_item(self,message_id):
        #update item with message_id .. need to find a way to make it more dynamic
        response = self.client.update_item(TableName=self.table_name,Key={
            self.p_key : {'S': message_id}
            },
            UpdateExpression="SET Updated=:value1",
            ExpressionAttributeValues={
                ':value1' : {'BOOL': True}
            }
        )
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            print(f"Succesfully updated message {message_id} on table {self.table_name}")
        else:
            print("Something went wrong updating item with key {message_id}")
            print(response)                  

"""
def main():

    dynamo = Dynamodb_helper('messages')
    #dynamo.put_item()
    #dynamo.get_item('ENUUTNLFQU')
    dynamo.update_item('ENUUTNLFQU')


if __name__ == "__main__":
    main()
"""
