resource "aws_sqs_queue" "SQSQueue_input" {
  name = var.sqs_q_name
  tags = var.default_tags
}

resource "aws_sqs_queue_policy" "SQSQueue_input_policy" {

  queue_url = aws_sqs_queue.SQSQueue_input.id
  policy = <<POLICY
{
 "Version": "2012-10-17",
 "Id": "sqspolicy",
 "Statement": [
  {
   "Sid": "first",
   "Effect": "Allow",
   "Principal": {
     "Service": "s3.amazonaws.com"
   },
   "Action": [
    "SQS:SendMessage"
   ],
   "Resource": "${aws_sqs_queue.SQSQueue_input.arn}",
   "Condition": {
      "ArnLike": { "aws:SourceArn": "${aws_s3_bucket.s3b[0].arn}" }
   }
  }
 ]
}
POLICY
}

