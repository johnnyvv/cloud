variable "default_tags" {
  type        = map
  description = "Used as default tags on resources. Default it sets app tag"
  default     = {
    app = "s3-sqs-ddb"
  }
}

variable "aws_region" {
  type        = string
  default     = "us-east-1"
  description = "The region for the AWS provider to use"
}

variable "s3_buckets" {
  type    = list(string)
  default = ["delme-jvvx-input","delme-jvvx-output"]
}

variable "sqs_q_name" {
  type    = string
  default = "sqsQS3Input"
}

variable "dynamo_table_name" {
  type        = string
  default     = "Messages"
  description = "The name of the dynamodb table to create"
}

variable "dynamo_table_hash_key" {
  type        = string
  default     = "MessageId"
  description = "The hash key to use"
}

variable "param_input_name" {
  type    = string
  default = "s3_bucket_name_input"
}

variable "param_output_name" {
  type    = string
  default = "s3_bucket_name_output"
}

variable "param_sqs_q_name" {
  type    = string
  default = "s3_queue_name"
}

variable "param_dynamo_table_name" {
  type = string
  default = "s3_dynamo_table_name"
}
