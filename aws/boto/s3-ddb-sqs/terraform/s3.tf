
resource "aws_s3_bucket" "s3b" {
  count = length(var.s3_buckets)

  bucket        = var.s3_buckets[count.index]
  force_destroy = true
  acl           = "private"
  
  tags = var.default_tags
}

resource "aws_s3_bucket_public_access_block" "s3bPa" {
  count = length(var.s3_buckets)
  
  bucket                  = aws_s3_bucket.s3b[count.index].id
  block_public_acls       = true
  block_public_policy     = true
  restrict_public_buckets = true
  ignore_public_acls      = true
}

resource "aws_s3_bucket_notification" "bucket_notification_input" {

  bucket = aws_s3_bucket.s3b[0].id
   
  queue {
    queue_arn = aws_sqs_queue.SQSQueue_input.arn 
    events    = ["s3:ObjectCreated:Put"] #https://docs.aws.amazon.com/AmazonS3/latest/dev/NotificationHowTo.html#notification-how-to-event-types-and-destinations
  }
}
