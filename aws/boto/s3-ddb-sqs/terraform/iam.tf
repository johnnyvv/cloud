/*
items
instance profile for putting objects in input bucket
instance profile for getting objects from input bucket, poll sqs, delete messages, put items in ddb
*/

resource "aws_iam_instance_profile" "putter" {
  name = "putter_profile"
  role = aws_iam_role.putter.name
}

resource "aws_iam_instance_profile" "poller" {
  name = "poller_profile"
  role = aws_iam_role.poller.name
}

resource "aws_iam_role_policy_attachment" "putter_role_attach" {
  role       = aws_iam_role.putter.name
  policy_arn = aws_iam_policy.putter_policy.arn
}

resource "aws_iam_role_policy_attachment" "poller_role_attach" {
  role       = aws_iam_role.poller.name
  policy_arn = aws_iam_policy.poller_policy.arn

}

resource "aws_iam_role" "putter" {
  name = "putter_role"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role" "poller" {
  name = "poller_role"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "putter_policy" {
  name	      =	"PutterPolicy"
  description = "Policy for allowing to put objects in input bucket and get ssm parameter"

  policy = <<EOF
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Sid":"statement1",
         "Effect":"Allow",
         "Action":[ 
            "s3:ListBucket", 
            "s3:PutObject",
            "s3:GetObject"  
         ],
         "Resource":[
            "${aws_s3_bucket.s3b[0].arn}",
            "${aws_s3_bucket.s3b[0].arn}/*"
         ]
       },
       {
          "Effect": "Allow",
          "Action": [
            "ssm:GetParameter"
            ],
          "Resource": "${aws_ssm_parameter.S3InputBucket.arn}"
          }
    ]
}
EOF
}

resource "aws_iam_policy" "poller_policy" {
  name        = "PollerPolicy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ssm:GetParameters"
            ],
            "Resource": [
                "${aws_ssm_parameter.S3InputBucket.arn}",
                "${aws_ssm_parameter.S3OutputBucket.arn}",
                "${aws_ssm_parameter.SqsQueue.arn}",
                "${aws_ssm_parameter.DynamoTableName.arn}"
            ]
        },
        {
            "Sid": "sqs1",
            "Effect": "Allow",
            "Action": [
                "sqs:ReceiveMessage",
                "sqs:DeleteMessage",
                "sqs:GetQueueAttributes",
                "sqs:GetQueueUrl"
            ],
            "Resource": [
                "${aws_sqs_queue.SQSQueue_input.arn}"
                ]
        },
        {
            "Sid": "sqs2",
            "Effect": "Allow",
            "Action": "sqs:ListQueues",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:ListBucket"
            ],
            "Resource": [
                "${aws_s3_bucket.s3b[0].arn}",
                "${aws_s3_bucket.s3b[0].arn}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:ListBucket",
                "s3:PutObject"
            ],
            "Resource": [
                "${aws_s3_bucket.s3b[1].arn}",
                "${aws_s3_bucket.s3b[1].arn}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
              "dynamodb:*"
            ],
            "Resource": [
                "${aws_dynamodb_table.s3_dynamo_table.arn}"
            ]
        }
    ]
}
EOF
}
