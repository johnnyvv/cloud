resource "aws_ssm_parameter" "S3InputBucket" {
  name  = var.param_input_name
  type  = "String"
  value = var.s3_buckets[0]
}

resource "aws_ssm_parameter" "S3OutputBucket" {
  name  = var.param_output_name
  type  = "String"
  value = var.s3_buckets[1]
}

resource "aws_ssm_parameter" "SqsQueue" {
  name  = var.param_sqs_q_name
  type  = "String"
  value = var.sqs_q_name
}

resource "aws_ssm_parameter" "DynamoTableName" {
  name  = var.param_dynamo_table_name
  type  = "String"
  value = var.dynamo_table_name
}
