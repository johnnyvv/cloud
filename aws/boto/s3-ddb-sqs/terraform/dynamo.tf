resource "aws_dynamodb_table" "s3_dynamo_table" {
  name = var.dynamo_table_name
  billing_mode = "PROVISIONED"
  read_capacity = "3"
  write_capacity = "2"
  hash_key = var.dynamo_table_hash_key

  attribute {
    name = "MessageId"
    type = "S"
  }

  tags = var.default_tags
}
