import boto3
import os

from tempfile import gettempdir
from datetime import datetime
tmp_folder = gettempdir()

#create s3 
s3 = boto3.resource('s3')
bucket = s3.Bucket("delme-jvv")


#list all objects
for obj in bucket.objects.all():
    print(obj.key)


#put object
# you can put an object or upload, we will put
obj_name = datetime.now().strftime("%H-%M-%d-%m-%Y")

#https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Bucket.upload_file
object = bucket.put_object(
        Key=(f"{obj_name}.txt"),
        Body=obj_name.encode()
        )
print(f"Put object {object.key}")