## Example script ##

This is an example project for myself using 

* The script runs without access id / key
* Gets a parameter from system manager

* policies / roles to access AWS services
* ECS to run container
* puts an object in a bucket 
* Bucket name is a parameter in System Manager 


To do:

* Image is quiet large, probably better of using another image  -> need to check


### How to use ###

The script can run on ec2 instances or on containers using a task role
You need to make that the proper policies are attached to the roles used
In this directory you will find the policies