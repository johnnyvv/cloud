import boto3
from os import environ
from socket import gethostname
from time import sleep
from tempfile import gettempdir
from datetime import datetime

tmp_folder = gettempdir()
files_put=0

#authenticate to AWS

##print auth 
identity = boto3.client('sts')

for k,v in identity.get_caller_identity().items():
    if k == "ResponseMetadata":
        break
    print(f"{k}:{v}")
del identity

#Now we use env parameter value, need to check what permissions needed for IAM
#Set env variable s3_bucket_paramater to the bucket to use
#Region is env variable as well, might make a check against aws to see if region is valid
#param_client = boto3.client('ssm', region_name='us-east-1')
param_client = boto3.client('ssm', region_name=environ.get('AWS_REGION','us-east-1'))
bucket_name = param_client.get_parameter(Name=environ.get('s3_bucket_parameter'))['Parameter']['Value']
del param_client

#env variable or use parameter store to retrieve value for bucket

#create s3 
s3 = boto3.resource('s3')
bucket = s3.Bucket(bucket_name)

#list all objects
for obj in bucket.objects.all():
    print(obj.key)

#put object
# you can put an object or upload, we will put
while True:
    print(f"Files put to s3 {files_put}")
    obj_name = f'{gethostname()}-{datetime.now().strftime("%H-%M-%d-%m-%Y")}'

    #https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Bucket.upload_file
    object = bucket.put_object(
            Key=(f"{gethostname()}/{obj_name}.txt"),
            Body=obj_name.encode()
            )
    print(f"Put object {object.key}")
    files_put+=1
    sleep(int(environ.get('SLEEP_TIME',60)))