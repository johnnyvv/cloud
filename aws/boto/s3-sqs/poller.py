import boto3, json
from time import sleep
from os import environ


def main():

    #pylint: disable=no-member

    param_client = boto3.client('ssm', region_name=environ.get('AWS_REGION','us-east-1'))
    sqs_queue=param_client.get_parameter(Name='s3_queue_name')['Parameter']['Value']
    s3_output_bucket = param_client.get_parameter(Name='s3_bucket_name_output')['Parameter']['Value']
    del param_client

    sqs = boto3.resource('sqs', region_name=environ.get('AWS_REGION','us-east-1'))
    sqs.get_queue_by_name(QueueName=sqs_queue)
    sqs_url = sqs.get_queue_by_name(QueueName=sqs_queue).url

    s3 = boto3.resource('s3')
    output_bucket = s3.Bucket(s3_output_bucket)
    del s3

    queue = sqs.Queue(sqs_url)
    max_msgs = 10

    #checking queue
    client = boto3.client('sqs', region_name=environ.get('AWS_REGION','us-east-1'))
    queue_attr = client.get_queue_attributes(
        QueueUrl=sqs_url,
        AttributeNames=['ApproximateNumberOfMessages'])

    print(f"Items in queue: {queue_attr['Attributes']['ApproximateNumberOfMessages']}")
    #checking for messages

    #connecting to s3 bucket to read message
    s3c = boto3.client('s3')


    for message in queue.receive_messages(
            MaxNumberOfMessages=max_msgs):
            
        print(f"Processing message {message.message_id}") 
        body = json.loads(message.body)

        if "Records" in body:

            print(f"object in bucket {body['Records'][0]['s3']['bucket']['name']} - {body['Records'][0]['s3']['object']['key']}")
            #create an object from the bucket name and key in the message - 
            obj = s3c.get_object(
                    Bucket=body['Records'][0]['s3']['bucket']['name'],
                    Key=body['Records'][0]['s3']['object']['key']
                    )

            output_bucket.put_object(
                    Key=body['Records'][0]['s3']['object']['key'],
                    Body=obj['Body'].read().upper()
                    )
            message.delete()
        
        elif "Event" in body:
            if body['Event'] == "s3:TestEvent":
                print("Test message found")
                print(f"Test message from {body['Service']} at {body['Time']}")
                message.delete()
                print("Deleted")
        
        else:
            print("Did not find s3 in message, please check if processing is needed")
            print(json.dumps(body,indent=4, sort_keys=True))

if __name__ == '__main__':
    main()
"""
    obj = s3.head_object(
            Bucket='testbucket10101x',
            Key=str("xbun/xbun-21-36-26-03-2020.txt")
            )
"""