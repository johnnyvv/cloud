## Sample script using s3, sqs and ssm ##

### Create environment ###
In the root of this project, there is a file named "create-env.sh"

In this script you need to set:
* Bucket name for input/output
* Edit queue name, if the one in the script doesnt fit your thoughts
* Edit parameter names, if the one in the script doesnt fit your thoughts

This will create the following:
* S3 buckets for input / output 
* Ssm parameters for input/output buckets and queuename
* Creates SQS queue 
* Create Queue policy json file
* Assign Queue policy json file to queue that allows input bucket to send messages
* Create bucket notification json
* Assign bucket notitication json to input bucket

### To do ### 
* Generate EC2 / ECS policies
* Create instance role for ec2 / container role for ecs 