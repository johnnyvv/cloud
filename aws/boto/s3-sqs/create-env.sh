#need to add error checking later
bucket_name_input="delme-jvv-input"
bucket_name_output="delme-jvv-output"
bucket_event_name="S3SQSEvent-${bucket_name_input}"
ssm_param_name_input="s3_bucket_name_input"
ssm_param_name_output="s3_bucket_name_output"
ssm_param_name_queue="s3_queue_name"
region=$(aws configure get region)
account_id=$(aws sts get-caller-identity --query Account --output text)

#region=$(grep 'region'  ~/.aws/config | awk -F "= " '{print $2}')

#create bucket
aws s3api create-bucket --bucket $bucket_name_input --acl private
aws s3api create-bucket --bucket $bucket_name_output --acl private
aws s3api put-public-access-block --bucket $bucket_name_input --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
aws s3api put-public-access-block --bucket $bucket_name_output --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

#create ssm paramater
aws ssm put-parameter --name $ssm_param_name_input --value $bucket_name_input --type String
aws ssm put-parameter --name $ssm_param_name_output --value $bucket_name_output --type String
aws ssm put-parameter --name $ssm_param_name_queue --value $bucket_event_name --type String

#create sqs queue
q_url=$(aws sqs create-queue --queue-name S3SQSEvent-delme-jvv-input --region $region --output text)
q_arn=$(aws sqs get-queue-attributes  --queue-url $q_url --attribute-names QueueArn | jq -r ".Attributes.QueueArn")

#need to attach policy to queue and create s3 notification to sqs

cat > queue-pol.json << EOF
{
"Policy": "{\"Version\":\"2008-10-17\",\"Id\":\"example-id\",\"Statement\":[{\"Sid\":\"TestSid\",\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"s3.amazonaws.com\"},\"Action\":[\"SQS:SendMessage\"],\"Resource\":\"${q_arn}\",\"Condition\":{\"ArnLike\":{\"aws:SourceArn\":\"arn:aws:s3:*:*:${bucket_name_input}\"}}}]}"
}
EOF

aws sqs set-queue-attributes --queue-url $q_url --attributes file://queue-pol.json

cat > s3-event-add.json << EOF
{
  "QueueConfigurations": [
    {
      "Id": "Sqs",
      "QueueArn": "${q_arn}",
      "Events": ["s3:ObjectCreated:Put"]
    }
  ]
}
EOF

aws s3api put-bucket-notification-configuration --bucket $bucket_name_input --notification-configuration file://s3-event-add.json