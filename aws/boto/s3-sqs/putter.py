import boto3
from os import environ
from socket import gethostname
from time import sleep
from datetime import datetime


def main():

    files_put=0

    ##print auth 
    identity = boto3.client('sts')
    for k,v in identity.get_caller_identity().items():
        if k == "ResponseMetadata":
            break
        print(f"{k}:{v}")
    del identity

    #connect to s3 bucket
    #first we will need to get  the bucket name through ssm paramaters
    param_client = boto3.client('ssm', region_name=environ.get('AWS_REGION','us-east-1'))
    bucket_name = param_client.get_parameter(Name=environ.get('s3_bucket_name_input','s3_bucket_name_input'))['Parameter']['Value']
    del param_client

    print(f"Bucket name is {bucket_name}")
    
    #pylint: disable=no-member
    #we will put a message every x seconds
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)

    while True:
        print(f"Files put to s3 {files_put}")
        obj_name = f'{gethostname()}-{datetime.now().strftime("%H-%M-%d-%m-%Y")}'

        #https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Bucket.upload_file
        object = bucket.put_object(
                Key=(f"{gethostname()}/{obj_name}.txt"),
                Body=f"Hello from me {obj_name}".encode()
                )
        print(f"Put object {object.key}")
        files_put+=1
        sleep(int(environ.get('SLEEP_TIME',60)))

if __name__ == "__main__":
    main()