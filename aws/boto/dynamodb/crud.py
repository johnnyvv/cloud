#sample for creating, reading, updating and delete in dynamodb
#you will need a table users -> email,username,fName,lName, userActive

import boto3, json,random

TABLE_NAME = 'users'

def main():
    
    #create the ddb client to interact to the table with
    client = boto3.client('dynamodb')
    email_list = ['dunno@me.local','hello@daar.doei']
#getting items
#    print(json.dumps(get_item(client,test_email)))
#    print_item(get_item(client,email_list))
#    get_items(client)
#    print_items(get_items(client))

    #update_item(client,'hello@daar.doei')
    update_item(client)

def get_item(client,email_list):
    #you can get a single item
    #will give you the item that matches the primary key you sent with the initial request
    #will return a json object, you can access values from response['Item']['attributeX']
    response = client.get_item(TableName=TABLE_NAME,
            Key={
                'email': { 'S': email_list[random.randint(0,(len(email_list)-1))]}
                },
            AttributesToGet=[
                'email',
                'username'
                ]
            )
    return response

def get_items(client):
    #you can 100 items with max size combined of 16MB of data - if exceed will return partial resultset
    #to do is create a request auto filled with variables / keys
    response = client.batch_get_item(RequestItems={
        TABLE_NAME: {
            'Keys':[
                    {
                        'email': {'S': 'hello@daar.doei'}
                    },{
                        'email': {'S': 'dunno@me.local'}
                        }
                ],
            'AttributesToGet': [
                'email',
                'username',
                'password'
                ]
            }
        }
    )
    return response

def print_item(response):
    #print key,value pairs in return object from get_item(s)
    for k,v in response['Item'].items():
        for key,value in v.items():
            print(f"{k} => {value}")


def print_items(response):
    #get_batch_item returns json object Responses.Users which is a list that containers map for every key asked (up to maximum ofcourse)
    for user in response['Responses'][TABLE_NAME]:
        for k,v in user.items():
            for key,value in v.items():
                print(f"{k} => {value}")

def update_item(client):
    #to do -> make it kind of dynamic -> give it a key, attribute to update and the value of that attribute
    response = client.update_item(
            TableName=TABLE_NAME,
            Key={
                'email': { 'S': 'dunno@me.local'}
                },
            UpdateExpression="SET password=:value1, isAdmin=:value2", #colon is a placeholder, you will use this in the ExpressionAttributeValues
            ExpressionAttributeValues={
                ':value1': {'S': 'UPDATEDPASSWORDBRO'},
                ':value2': {'BOOL': False}
                }
    )

if __name__ == '__main__':
    main()

