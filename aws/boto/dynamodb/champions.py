import boto3,json


country_map = {"eredivisie": {"code":"NL","cross_country": False},"bundesliga": {"code":"DE","cross_country": False},"cl":{ "cross_country": True, "code":"EU"}}

def main():

    client = boto3.client('dynamodb')
    table_name = 'champions'
#    put_item(client,'champions')
    put_items(client,'champions')

def get_input():
    competition = str(input("competition: "))
    season = str(input("season: "))
    winner = str(input("winner: "))
    return {"competition":competition, "season":season, "winner":winner, "country":country_map.get(competition)}

def put_item(client,table_name):
    #here we can put one item at once into the table
    to_put = get_input()
    response = client.put_item(TableName=table_name,Item={
        'competition': {'S': to_put.get('competition')},
        'season': {'S': to_put.get('season')},
        'winner': {'S': to_put.get('winner')},
        'country': {'S': country_map.get(to_put.get('competition'))['code']},
        'crossCountry': { 'BOOL': country_map.get(to_put.get('competition'))['cross_country']}
        })

def put_items(client,table_name):
    #put multiple items at once, for limits check docs
    response = client.batch_write_item(RequestItems={
        table_name: [
            {'PutRequest': {
                'Item': {
                   'competition': {'S': 'bundesliga'},
                   'season': { 'S': '2017-2018'},
                   'winner': { 'S': 'bayern munchen'},
                   'country': {'S': country_map.get('bundesliga')['code']},
                   'crossCountry': {'BOOL': country_map.get('bundesliga')['cross_country']}
                }}},
            {'PutRequest': {
                'Item': {
                   'competition': {'S': 'bundesliga'},
                   'season': { 'S': '2016-2017'},
                   'winner': { 'S': 'bayern munchen'},
                   'country': {'S': country_map.get('bundesliga')['code']},
                   'crossCountry': {'BOOL': country_map.get('bundesliga')['cross_country']}
                }}},
            {'PutRequest': {
                'Item': {
                   'competition': {'S': 'bundesliga'},
                   'season': { 'S': '2015-2016'},
                   'winner': { 'S': 'bayern munchen'},
                   'country': {'S': country_map.get('bundesliga')['code']},
                   'crossCountry': {'BOOL': country_map.get('bundesliga')['cross_country']}
                }}},
            ]}
        )


#    print(json.dumps(to_put))
if __name__ == '__main__':
    main()