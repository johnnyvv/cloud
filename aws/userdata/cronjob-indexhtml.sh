#!/bin/bash
#generate index.html for nginx on startup
html_path=/var/www/html
private_ip=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)
instance_id=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)

if [[ -f "${html_path}/index.html" ]] ; then echo "Index.html already present, exiting..." && exit 1 ; fi

cat << EOF >> $html_path/index.html
id: $instance_id
private ip: $private_ip
EOF

systemctl start nginx

#cronjob -e 
#@reboot bash /root/create-index.sh
# echo "@reboot bash /root/create-index.sh" > /var/spool/cron/crontabs/root #maybe overrides entries already present..